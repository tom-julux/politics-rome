using UnityEngine;
[System.Serializable]
public class Voter
{
    public Tendenzies opinion;
    public string name;

    public Voter(float privacy, float war, float individualism, float strongLeader)
    {
        this.opinion = new Tendenzies(privacy, war, individualism, strongLeader);
    }
}