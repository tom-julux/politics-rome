using UnityEngine;
[System.Serializable]
public class Perk
{
    public int monthlyCosts = 0;
    public int initialCosts = 0;
    public int popularityInitial = 0;
    public int popularityMonthly = 0;
    public Tendenzies tendenzies;
    public Tendenzies apperance;
    public Tendenzies penaltyTendenzies;
    [TextArea(3, 10)]
    public string description;

    public Perk()
    {
        this.tendenzies = Tendenzies.Empty;
        this.penaltyTendenzies = Tendenzies.Empty;
        this.apperance = Tendenzies.Empty;
    }
    public Perk(int popularityInitial, int popularityMonthly) :this()
    {
        this.popularityInitial = popularityInitial;
        this.popularityMonthly = popularityMonthly;
    }
    public virtual void OnInitialise(State state)
    {
        state.money -= this.initialCosts;
        state.popularity += this.popularityInitial;
        state.leaderOpinion += apperance;
        state.ShiftAll(this.tendenzies);
    }
    public virtual void OnRemove(State state)
    {
        state.ShiftAll(this.penaltyTendenzies);
    }
}
[System.Serializable]
public class WarPerk : Perk
{

    WarPerk() : base()
    {
    }
    public override void OnInitialise(State state)
    {
        state.money -= this.initialCosts;
        state.leaderOpinion += apperance;
        state.popularity += this.popularityInitial;
        state.atWar = true;
    }
    public override void OnRemove(State state)
    {
        state.atWar = false;
    }
}