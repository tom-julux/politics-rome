using UnityEngine;
public class WarPerkToggle : MonoBehaviour
{
    [SerializeField]
    public WarPerk perk;
    public void toggle(bool value)
    {
        GameObject.FindGameObjectWithTag("StateManager").GetComponent<StateManager>().togglePerk(perk);
    }
    public void showDescription()
    {
        GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>().setDiscription(perk);
    }
}