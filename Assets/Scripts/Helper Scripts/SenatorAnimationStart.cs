﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SenatorAnimationStart : MonoBehaviour
{

    public float StartOffsetTime;
    void Start()
    {
        Invoke("startPlaying", StartOffsetTime);
    }

    void startPlaying()
    {
        GetComponent<Animator>().SetBool("Talking", true);
        Destroy(this);
    }
}
