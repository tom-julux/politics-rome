﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Levelloader : MonoBehaviour
{
    public int targetLevel;
    private bool loading = false;
    public Text progressTarget;
    private AsyncOperation loadingOperation;
    public void PlayScene()
    {
        if (loading)
            return;
        loadingOperation = SceneManager.LoadSceneAsync(targetLevel, LoadSceneMode.Single);
        loadingOperation.allowSceneActivation = true;
        loading = true;
    }
    void onGui()
    {
        if (loading)
        {
            if (progressTarget == null)
                return;
            progressTarget.text = "Progress: " + loadingOperation.progress + "%";
        }
    }

}
