﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SchreiberAnimationStart : MonoBehaviour
{

    public float StartOffsetTime;
    void Start()
    {
        Invoke("startPlaying", StartOffsetTime);
    }

    void startPlaying()
    {
        GetComponent<Animator>().SetTrigger("start");
        GetComponent<AudioSource>().Play();
        Destroy(this);
    }
}
