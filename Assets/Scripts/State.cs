using System.Collections.Generic;
using UnityEngine;

public abstract class State
{
    public List<Voter> voters;
    protected Perk difficulty;

    public int money = 100;
    public bool atWar = false;
    public int popularity = 0;
    public float support = 0f;
    public float agreement = 0f;

    public Tendenzies leaderOpinion;

    public List<Perk> perks;
    public State(Tendenzies tendenzies, float scale, int money)
    {
        voters = new List<Voter>();
        for (int i = 0; i < 100; i++)
        {
            float privacy = Mathf.Lerp(Calulate((float)i, scale, Random.Range(0, 100)), tendenzies.privacy < 0 ? -1 : 1, tendenzies.privacy);
            float war = Mathf.Lerp(Calulate((float)i, scale, Random.Range(0, 100)), tendenzies.war < 0 ? -1 : 1, tendenzies.war);
            float individualism = Mathf.Lerp(Calulate((float)i, scale, Random.Range(0, 100)), tendenzies.individualism < 0 ? -1 : 1, tendenzies.individualism);
            float strongLeader = Mathf.Lerp(Calulate((float)i, scale, Random.Range(0, 100)), tendenzies.strongLeader < 0 ? -1 : 1, tendenzies.strongLeader);
            voters.Add(new Voter(privacy, war, individualism, strongLeader));
        }
        perks = new List<Perk>();
        this.money = money;
        difficulty = new Perk(0, 0);
        perks.Add(difficulty);
    }
    public void ShiftAll(Topics t, float amount)
    {
        switch (t)
        {
            case Topics.Privacy:
                foreach (Voter voter in voters)
                    voter.opinion.privacy = Mathf.Clamp(voter.opinion.privacy + amount, 0, 1);
                break;
            case Topics.War:
                foreach (Voter voter in voters)
                    voter.opinion.war = Mathf.Clamp(voter.opinion.war + amount, 0, 1);
                break;
            case Topics.Individualism:
                foreach (Voter voter in voters)
                    voter.opinion.individualism = Mathf.Clamp(voter.opinion.individualism + amount, 0, 1);
                break;
            case Topics.StrongLeader:
                foreach (Voter voter in voters)
                    voter.opinion.strongLeader = Mathf.Clamp(voter.opinion.strongLeader + amount, 0, 1);
                break;
        }
    }
    public void ShiftAll(Tendenzies t)
    {
        foreach (Voter voter in voters)
            voter.opinion += t;
    }

    public abstract StateStates HandleElection(int TimeInOffice);
    public void HandlePerks()
    {
        foreach (Perk perk in perks)
        {
            money -= perk.monthlyCosts;
            popularity += perk.popularityMonthly;
        }
    }
    public void AddPerk(Perk perk)
    {
        if (perks.Contains(perk))
            return;


        perk.OnInitialise(this);

        perks.Add(perk);
    }
    public void RemovePerk(Perk perk)
    {
        if (!perks.Contains(perk))
            return;
        perk.OnRemove(this);
        perks.Remove(perk);
    }
    Voter getRandomMan()
    {
        return voters[Random.Range(0, voters.Count)];
    }
    public static float Calulate(float x, float scale, float y)
    {
        return (Mathf.PerlinNoise(x / scale, y) - 0.5f) * 2f;
    }
    public static float Calulate(float x, float scale)
    {
        return Calulate(x, scale, 0f);
    }
    public float calcAgreement()
    {
        float sum = 0f;
        foreach (Voter voter in voters)
            sum += Tendenzies.Compare(leaderOpinion, voter.opinion);
        agreement = 1 - (sum / voters.Count / 2);
        return agreement;
    }
    public float calcExpenses()
    {
        float sum = 0f;
        foreach (Perk perk in perks)
            sum += perk.monthlyCosts >= 0 ? perk.monthlyCosts : 0;
        return sum;
    }
    public int calcTedenzie()
    {

        int sum = 0;
        foreach (Perk perk in perks)
            sum += perk.popularityMonthly;
        return sum;
    }

    public float calcDonations()
    {
        float sum = 0f;
        foreach (Perk perk in perks)
            sum += perk.monthlyCosts <= 0 ? -perk.monthlyCosts : 0;
        return sum;
    }
    public float calcSupport()
    {
        float sum = 0f;
        foreach (Voter v in voters)
            sum += Tendenzies.CompareAbs(leaderOpinion, v.opinion) >= 0 ? 1 : 0;
        support = Mathf.Clamp((sum + popularity) / voters.Count, 0, 1);
        return support;
    }
}