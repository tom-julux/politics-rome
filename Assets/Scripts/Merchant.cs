using UnityEngine;

public class Merchant : IInteractable
{
    public StateManager stateManager;

    override
    public void Interact()
    {
        stateManager.state.ShiftAll(Topics.Individualism, 0.1f);
        stateManager.state.money -= 10;
    }
}