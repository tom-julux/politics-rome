﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public Text moneyText;
    public Text tendenzyTexy;
    public Text tioText;
    public Text popularityText;
    public Text donationsText;
    public Text expensesText;
    public Text supportText;
    public Text agreementText;
    public Slider countdownSlider;
    public GameObject perksMenu;
    public Text perksInfoCosts;
    public Text perksInfoPopuarity;
    public Text perksInfoDonations;
    public Text perksInfoExpenses;
    public Text perksInfoPopularityOverTIme;
    public Text perksDiscirption;
    public void setSupportText(float value)
    {
        supportText.text = "Support: " + (value < 1 ? value.ToString("0.##\\%") : "100%");
    }
    public void setAgreementText(float value)
    {
        agreementText.text = "Agreement: " + (value < 1 ? value.ToString("0.##\\%") : "100%");
    }
    public void setMoneyText(int value)
    {
        moneyText.text = "Money: " + value;
    }
    public void setExpensesText(float value)
    {
        expensesText.text = "Expenses: " + value;
    }
    public void setDonationsText(float value)
    {
        donationsText.text = "Donations: " + value;
    }
    public void setPopularirtyText(int value)
    {
        popularityText.text = "Popularity: " + value;
    }

    public void setTIO(int value)
    {
        tioText.text = "Time in Office: " + value + "M";
    }
    public void setCountdownSlider(float value)
    {
        countdownSlider.value = value;
    }
    public void setTendenzyTexy(int value)
    {
        tendenzyTexy.text = "Tendenzie: " + value;
    }
    public void togglePerksMenu()
    {
        perksMenu.SetActive(!perksMenu.activeSelf);
        perksDiscirption.text = "";
    }
    public void hidePerksMenu()
    {
        perksMenu.SetActive(false);
    }
    public void setDiscription(Perk p)
    {
        perksDiscirption.text = p.description;
        perksInfoCosts.text = "" + p.initialCosts;
        perksInfoPopuarity.text = "" + p.popularityInitial;
        perksInfoPopularityOverTIme.text = "" + p.popularityMonthly;
        perksInfoDonations.text = "" + (p.monthlyCosts < 0 ? -p.monthlyCosts : 0);
        perksInfoExpenses.text = "" + (p.monthlyCosts > 0 ? -p.monthlyCosts : 0);

    }
}
