using System.Collections;
using UnityEngine;

 public class DialogueReference : IInteractable
    {
        public Dialogue dialogue;

        public override void Interact()
    {
          FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
     }

}
