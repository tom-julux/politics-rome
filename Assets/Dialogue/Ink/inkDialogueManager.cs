using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using Ink.Runtime;

public class inkDialogueManager : MonoBehaviour
{

    public Text nameText;
    public Text dialogueText;
    public GameObject dialogueChoicesParent;
    public GameObject choiceButtonPrefab;

    [SerializeField]
    private TextAsset storyFile;

    private Story _inkStory;
    public GameObject DialoguePanel;
    public StateManager stateManager;
    public UIManager ui;

    // Use this for initialization
    void Start()
    {
        _inkStory = new Story(storyFile.text);
        _inkStory.BindExternalFunction("addPopularity", (int arg1) =>
        {
            stateManager.state.popularity += arg1;
        });
        _inkStory.BindExternalFunction("getPopularity", () =>
        {
            return stateManager.state.popularity;
        });
        _inkStory.BindExternalFunction("shiftPopulation", (string a) =>
        {
            float[] split = Array.ConvertAll(a.Split('@'), s => float.Parse(s));
            stateManager.state.ShiftAll(new Tendenzies(split[0], split[1], split[2], split[3]));
        });
        _inkStory.BindExternalFunction("shiftLeader", (string a) =>
        {
            float[] split = Array.ConvertAll(a.Split('@'), s => float.Parse(s));
            stateManager.state.leaderOpinion += new Tendenzies(split[0], split[1], split[2], split[3]);
        });
        _inkStory.BindExternalFunction("addMoney", (int a) =>
        {
            stateManager.state.money -= a;
        });
    }

    public void StartDialogue(string reference)
    {
        StartDialogue(reference, "");
    }
    public void StartDialogue(string reference, string name)
    {
        ui.hidePerksMenu();
        _inkStory.ChoosePathString(reference);
        removeChoices();
        DialoguePanel.SetActive(true);
        if (name != "")
        {
            nameText.gameObject.SetActive(true);
            nameText.text = name;
        }
        else
        {
            nameText.gameObject.SetActive(false);
        }
        DisplayNextSentence();
    }
    public void StartDialogue(string reference, bool resetCallstack)
    {
        if (resetCallstack)
            _inkStory.ResetCallstack();
        StartDialogue(reference);
    }

    public void DisplayNextSentence()
    {
        if (_inkStory.canContinue)
        {
            StopAllCoroutines();
            StartCoroutine(TypeSentence(_inkStory.Continue()));

            return;
        }
        if (_inkStory.currentChoices.Count > 0)
        {
            ShowChoices(_inkStory.currentChoices);
            return;
        }
        EndDialogue();

    }

    IEnumerator TypeSentence(string sentence)
    {
        if(sentence == "")
            DisplayNextSentence();
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    void EndDialogue()
    {
        DialoguePanel.SetActive(false);
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>().pause = false;

    }
    //TODO
    void ShowChoices(List<Choice> choices)
    {
        //just in case
        removeChoices();
        //INstanciate BUttons
        foreach (Choice c in choices)
        {
            GameObject instance = Instantiate(choiceButtonPrefab, Vector3.zero, Quaternion.identity);
            instance.transform.SetParent(dialogueChoicesParent.transform, true);
            instance.GetComponentInChildren<Text>().text = c.text;
            instance.GetComponent<Button>().onClick.AddListener(() => selectChoice(c.index));
            //dialogueChoicesParent.a
        }
    }
    void removeChoices()
    {

        for (int i = dialogueChoicesParent.transform.childCount - 1; i >= 0; i--)
            GameObject.Destroy(dialogueChoicesParent.transform.GetChild(i).gameObject);
    }
    public void selectChoice(int choice)
    {
        removeChoices();
        _inkStory.ChooseChoiceIndex(choice);
        DisplayNextSentence();
    }

    // Advanced Ink features
    public void bindExernalFunction(string name, Func<object> func)
    {
        _inkStory.BindExternalFunction(name, func);
    }
    public void unbindExernalFunction(string name)
    {
        _inkStory.UnbindExternalFunction(name);
    }
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            DisplayNextSentence();
        }
    }
}
