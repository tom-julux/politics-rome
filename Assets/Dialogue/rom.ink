LIST Topics = Privacy, war, Individualism, StrongLeader
VAR handled = false
EXTERNAL addPopularity(x)
EXTERNAL shiftPopulation(x)
EXTERNAL getPopularity()
EXTERNAL addMoney(x)
EXTERNAL shiftLeader(x)

=== Merchant ===

I am a merchant of writing tools as well as writing slaves. What can I do for you?
-(offering)
+  [Buy] -> buying
+  [What can you offer me?]
    I offer slaves and writing tools.
    So I can help you to gain popularity, by writing articles or explaining your ruling.
    You can also publish advertisements to shift the popular opinion on your side.
    Some options are also only available after you have enough popularity.
    -> Merchant.offering
+  [Cancel] ->DONE
==== buying ====
I offer you to preferably publish an article to boost your popularity and convince the people of your position or to advertise for you <>
and change the perception the people have of you. <>
{getPopularity() >= 100: But I could also make you a superstar...}
+ [Advertise]
    Ok, you can position yourself as one of the following persons.
    ++ [The unnamed solider.]
        This will cost you 20€.
        Do you want to purchase?
        +++ [Yes] {addPopularity(2)}<>
                  {addMoney(-20)}<>
                  {shiftLeader("0@0.1@0@0")}
        +++ [No] ->buying
    ++ [The strong Leader]
        This will cost you 20€.
        Do you want to purchase?
        +++ [Yes] {addPopularity(2)}<>
                  {addMoney(-20)}<>
                  {shiftLeader("0@0@0@0.05")}
        +++ [No] ->buying
    ++ [The philosopher king]
        This will cost you 20€.
        Do you want to purchase?
        +++ [Yes] {addPopularity(2)}<>
                  {addMoney(-20)}<>
                  {shiftLeader("0.05@0@0@0")}
        +++ [No] ->buying
    ++ [The fun guy]
        This will cost you 20€.
        Do you want to purchase?
        +++ [Yes] {addPopularity(2)}<>
                  {addMoney(-20)}<>
                  {shiftLeader("0@0@0.05@0")}
        +++ [No] ->buying
+ [Publish]
    OK, about what topic do you want to inform the public?
    ++ [War]
        This will cost you 20€.
        Do you want to purchase?
        +++ [Yes] {addPopularity(2)}<>
                  {addMoney(-20)}
                  Last Question: How do you want to talk about the topic?
            ++++ [Positiv]{shiftPopulation("0@0.05@0@0")}
            ++++ [Negativ]{shiftPopulation("0@-0.05@0@0")}
        +++ [No] ->buying
    ++ [Privacy]
        This will cost you 20€.
        Do you want to purchase?
        +++ [Yes] {addPopularity(2)}<>
                  {addMoney(-20)}
                  Last Question: How do you want to talk about the topic?
            ++++ [Positiv]{shiftPopulation("0.05@0@0@0")}
            ++++ [Negativ]{shiftPopulation("-0.05@0@0@0")}
        +++ [No] ->buying
    ++ [Individualism]
        This will cost you 20€.
        Do you want to purchase?
        +++ [Yes] {addPopularity(2)}<>
                  {addMoney(-20)}
                  Last Question: How do you want to talk about the topic?
                  ++++ [Positiv]{shiftPopulation("0@0@0.05@0")}
                  ++++ [Negativ]{shiftPopulation("0@0@-0.05@0")}
        +++ [No] ->buying
    ++ [Your Personality]
        This will cost you 20€.
        Do you want to purchase?
        +++ [Yes] {addPopularity(2)}<>
                  {addMoney(-20)}
                  Last Question: How do you want to talk about the topic?
                  ++++ [Positiv]{shiftPopulation("0@0@0@0.05")}
                  ++++ [Negativ]{shiftPopulation("0@0@0@-0.05")}
        +++ [No] ->buying
+ {getPopularity() >= 100} [Make me a star!]
        This will cost you 100€. But make you super popular.
        Do you want to purchase?
        +++ [Yes] {addPopularity(100)}<>
                  {addMoney(-100)}
        +++ [No] ->buying
+ [Return] -> Merchant
+ [Cancel] -> DONE
- Thank you for your purchase.
->DONE

=== Senator_0 ===
Hello, my ruler.
* [Ask for money] I am afraid my family is in the unfortunate position of not being able to fulfill your request.
    ** [Insist on money]
        I really can´t afford to invest money in your good cause.
        *** [Keep presuring] I would really like to give you money, but I can´t.
            **** [Threaten with prison] Even If I´d like to I could not give you money.
                ***** [Threaten with Death] Ok, fine here you have some. {addMoney(20)}
                ***** [Talk about politcs]
            **** [Talk about politcs]
        *** [Talk about politcs]
    ** [Talk about politcs]
* [Talk about politics]
- Politics...
I really love what you did for our country, but we need more morals.
* [Grant (+ 2 Popularity - 5 Money)] {addPopularity(2)}{addMoney(-5)}<>
* [Decline (- 1 Popularity)] {addPopularity(-1)}<>
- It´s so wonderful that you take the time to talk to us about what matters for us. -> DONE

=== Senator_1 ===
Hello my ruler, do you want to buy a drink? It´s only 1$.
+ [Yes] {addMoney(-1)}{addPopularity(1)} Delicious isn´t it?
+ [No]
-> DONE

=== Citizens ===
{ handled: There are currently no requests. ->DONE | <>}
There is a citizen that has a request for the sovereign. Do you want to help him?
* [Yes] He would like to kill his Christian neighbor and then take over his house.
    Do you general allow the murder of Christians?
    ** [Yes] {addPopularity(5)}{shiftPopulation("-0.05@0@0@0.02")}<>
    ** [No] Do you instead want to execute the man for asking this question?
        *** [Yes]{addPopularity(5)}{shiftPopulation("0@0@0@0.05")}<>
        *** [No]{addPopularity(2)}{shiftPopulation("0@0@0@-0.02")}<>
    -- Thank you for handeling this request.
    ~ handled = true
    ->DONE
* [No]
- ~ handled = true
-> DONE
